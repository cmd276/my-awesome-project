<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html>
<head>
<title><?php echo ucwords($vehicle); ?> Skins</title>
<style type="text/css">
body 
{
    background-image:url("<?=base_url('images/apoc.gif');?>");
    background-repeat: no-repeat;
    background-color:black;
    color:white;
}
div.main {
    text-align:center;
    border:0px solid white;
    position:fixed;
    left:230px;
    top:60px;
    bottom:10px;
    right:10px;
    overflow: auto;
}

div.menu
{
    position:fixed;
    left:10px;
    top:10px;
    width: 215px;
    border:0px solid white;
    bottom:10px;
    overflow-y: auto;
}
img.skin 
{
    width:128px;
    border:1px solid #a08400;
}
img.skin:hover
{
    width:128px;
    border:1px solid #e0c000;
}
div.image {
    display:inline-block;
    height:auto;
    border:0px solid black;
    padding:0px;
    margin:0px;
    margin-left:3px;
}

div.form
{
    position:fixed;
    left:240px;
    top:10px;
    height:40px;
}

div.menu ul li
{
    font-family: monospace;
    height:27px;
    padding:2px;
    text-align:center;
    font-size:x-large;
    background-image:url("<?=base_url('images/btn-ss.png');?>");
    background-repeat: no-repeat;
    color:#a08400;

}
div.menu ul li:hover
{
    background-image:url("<?=base_url('images/btn-ss-hover.png');?>");
    background-repeat: no-repeat;
    color:#e0c000;
}

div.menu ul li a:hover
{
        color:inherit;
}
div.menu ul li a
{
        color:inherit;
}

div.menu ul
{
    list-style-type: none;
    margin: 0px;
    padding: 0px;
}

</style>
</head>
<body>
<div class="menu">
<ul>
<li><a href="<?=site_url("judg");?>">Adjudicator</a></li>
<li><a href="<?=site_url("advo");?>">Advocate</a></li>
<li><a href="<?=site_url("apoc");?>">Apocalypse</a></li>
<li><a href="<?=site_url("aven");?>">Avenger</a></li>
<li><a href="<?=site_url("bans");?>">Banshee</a></li>
<li><a href="<?=site_url("basl");?>">Basilisk</a></li>
<li><a href="<?=site_url("bolo");?>">Bolo</a></li>
<li><a href="<?=site_url("disr");?>">Disruptor</a></li>
<li><a href="<?=site_url("drea");?>">Dreadlock</a></li>
<li><a href="<?=site_url("eman");?>">Emancipator</a></li>
<li><a href="<?=site_url("exec");?>">Executioner</a></li>
<li><a href="<?=site_url("goad");?>">Goad</a></li>
<li><a href="<?=site_url("gorg");?>">Gorgon</a></li>
<li><a href="<?=site_url("insp");?>">Insp</a></li>
<li><a href="<?=site_url("laza");?>">Laza</a></li>
<li><a href="<?=site_url("mino");?>">Minotaur</a></li>
<li><a href="<?=site_url("myrm");?>">Myrmidon</a></li>
<li><a href="<?=site_url("olyp");?>">Olympian</a></li>
<li><a href="<?=site_url("pala");?>">Paladin</a></li>
<li><a href="<?=site_url("pred");?>">Predator</a></li>
<li><a href="<?=site_url("prom");?>">Prometheus</a></li>
<li><a href="<?=site_url("qalm");?>">Qalm</a></li>
<li><a href="<?=site_url("bike");?>">Rebel Bike</a></li>
<li><a href="<?=site_url("recl");?>">Recluse</a></li>
<li><a href="<?=site_url("seek");?>">Seeker</a></li>
<li><a href="<?=site_url("shan");?>">Shan</a></li>
<li><a href="<?=site_url("shep");?>">Shepherd</a></li>
<li><a href="<?=site_url("talo");?>">Talon</a></li>
</ul>
</div>
<div class="form"><?php
$data = array(
    'name'          => 'skin',
    'id'            => 'skin',
);
$str1 = form_open_multipart(site_url("upload")); 
$str1 .= form_label("Add a skin:", "skin");
$str1 .= form_upload($data); 
$str1 .= form_close(); echo $str1; ?>
</div>
<div class="main">
<?php
$map = directory_map('./skins/',0);
if (isset($vehicle) && $vehicle != "")
{
    foreach ($map as $image)
    {
        if (is_array($image)) {}
        elseif ((strtolower (substr($image, 0, strlen($vehicle))) === strtolower ($vehicle)))
        {
            echo '<div class="image"><a href="' . base_url('skins/'.$image) . '"><img class="skin" src="' . base_url('skins/'.$image) . '"></a></div>';
        }
    }
}
?>
</div>
</body>
</html>
