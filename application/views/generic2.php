<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html>
<head>
<title><?php echo ucwords($vehicle); ?> Skins</title>
<style type="text/css">

body 
{
    background-image:url("<?=base_url('images/human_scramble.jpg');?>");
    background-repeat: no-repeat;
    background-color:black;
}

div.image {
    display:inline-block;
    height:auto;
    border:0px solid black;
    padding:1px;
    margin:1px;
}

div.main {
    text-align:center;
    margin-left:200px;
    border:0px solid white;
    position:fixed;
    left:111px;
    top:70px;
    width:240px;
    height:215px;
    overflow: auto;
}
img.skin 
{
    width:64px;
    border:1px solid black;
}
img.skin:hover
{
    width:64px;
    border:1px solid white;
}
div.menu
{
    position:fixed;
    left:80px;
    top:60px;
    width: 195px;
    border:0px solid white;
    height:300px;
    overflow-y: auto;
}

div.menu ul li
{
    font-family: monospace;
    height:27px;
    padding:2px;
    text-align:center;
    font-size:x-large;
    background-image:url("<?=base_url('images/btn-ss.png');?>");
    background-repeat: no-repeat;
    color:#a08400;

}
div.menu ul li:hover
{
    background-image:url("<?=base_url('images/btn-ss-hover.png');?>");
    background-repeat: no-repeat;
    color:#e0c000;
}

div.menu ul li a:hover
{
        color:inherit;
}
div.menu ul li a
{
        color:inherit;
}

div.menu ul
{
    list-style-type: none;
    margin: 0px;
    padding: 0px;
}

::-webkit-scrollbar {
    width: 1px;
}
</style>
</head>
<body>
<div class="menu">
<ul>
<li><a href="<?=site_url("advo");?>">Advocate</a></li>
<li><a href="<?=site_url("apoc");?>">Apocalypse</a></li>
<li><a href="<?=site_url("aven");?>">Avenger</a></li>
<li><a href="<?=site_url("bans");?>">Banshee</a></li>
<li><a href="<?=site_url("basl");?>">Basilisk</a></li>
<li><a href="<?=site_url("bike");?>">Rebel Bike</a></li>
<li><a href="<?=site_url("bolo");?>">Bolo</a></li>
<li><a href="<?=site_url("disr");?>">Disruptor</a></li>
<li><a href="<?=site_url("drea");?>">Dreadlock</a></li>
<li><a href="<?=site_url("eman");?>">Emancipator</a></li>
<li><a href="<?=site_url("exec");?>">Executioner</a></li>
<li><a href="<?=site_url("goad");?>">Goad</a></li>
<li><a href="<?=site_url("gorg");?>">Gorgon</a></li>
<li><a href="<?=site_url("insp");?>">Insp</a></li>
<li><a href="<?=site_url("judg");?>">Adjudicator</a></li>
<li><a href="<?=site_url("laza");?>">Laza</a></li>
<li><a href="<?=site_url("mino");?>">Minotaur</a></li>
<li><a href="<?=site_url("myrm");?>">Myrmidon</a></li>
<li><a href="<?=site_url("olyp");?>">Olympian</a></li>
<li><a href="<?=site_url("pala");?>">Paladin</a></li>
<li><a href="<?=site_url("pred");?>">Predator</a></li>
<li><a href="<?=site_url("prom");?>">Prometheus</a></li>
<li><a href="<?=site_url("qalm");?>">Qalm</a></li>
<li><a href="<?=site_url("recl");?>">Recluse</a></li>
<li><a href="<?=site_url("seek");?>">Seeker</a></li>
<li><a href="<?=site_url("shan");?>">Shan</a></li>
<li><a href="<?=site_url("shep");?>">Shepherd</a></li>
<li><a href="<?=site_url("talo");?>">Talon</a></li>
</ul>
</div>
<div class="main">
<?php
$map = directory_map('./skins/');
if (isset($vehicle) && $vehicle != "")
{
    foreach ($map as $image)
    {
        if ((strtolower (substr($image, 0, strlen($vehicle))) === strtolower ($vehicle)))
        {
            echo '<div class="image"><a href="' . base_url('skins/'.$image) . '"><img class="skin" src="' . base_url('skins/'.$image) . '"></a></div>';
        }
    }
}
?>
</div>
</body>
</html>
