<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Skins extends CI_Controller 
{
	public function index()
	{
		$this->load->helper('url');
		$page['vehicle'] = uri_string();
		$this->load->helper('directory');
		$this->output->enable_profiler(false);
		$this->load->helper('form');
		$this->load->view('generic', $page);
	}
}
